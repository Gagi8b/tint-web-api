﻿import { HttpModule } from '@angular/http';
import { LoginComponent } from './login.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MdButtonModule, MdInputModule } from '@angular/material';


@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [
        HttpModule,
        FormsModule,
        CommonModule,
        MdButtonModule,
        MdInputModule
    ],
    exports: [
        LoginComponent
    ]
})
export class LoginModule {
}