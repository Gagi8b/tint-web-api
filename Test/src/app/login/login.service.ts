﻿import { Headers, Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class LoginService {
    private apiUrl: string = 'http://localhost/AdInsure.Triglav.Kopaonik.API.Server/';

    constructor(private http: Http) {
    }

    public login(username: string, pass: string): Promise<void> {

        var body = 'grant_type=password&username=' + username + '&password=' + pass;

        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append('Access-Control-Allow-Origin', '*');

        return this.http.post(this.apiUrl + 'Token', body, {
            headers: headers
        })
            .toPromise()
            .then(response =>
                localStorage.setItem('token', response.text())
            )
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}