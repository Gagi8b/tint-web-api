﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { LoginService } from './login.service';

@Component({
    selector: 'login-component',
    templateUrl: './login.component.html',
    providers: [LoginService]
})
export class LoginComponent implements OnInit {
    public username: string;
    public password: string;
    private token: string;
    private msg: string;

    constructor(private loginService: LoginService, private router: Router) {
    }

    ngOnInit(): void {
    }

    public login(): void {
        this.msg = '';

        this.loginService.login(this.username, this.password).then(() => {
            this.router.navigateByUrl('/travel');
        }, (reason: any) => {
            this.msg = 'Invalid username or password.';
        });
    }
}