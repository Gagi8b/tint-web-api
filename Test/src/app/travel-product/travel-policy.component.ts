﻿import { Component, OnInit } from '@angular/core';
import { IZD0020001Type } from './domain/interfaces.gen';
import { IPersonType, RoleTypeId, IAddressType , AddressTypeIdType} from './domain/common-interfaces.gen';
import { TravelService } from './travel-product.service';


@Component({
    selector: 'travel-policy-component',
    templateUrl: './travel-policy.component.html',
    providers: [TravelService]
})
export class TravelPolicyComponent implements OnInit {
    public policyData: IZD0020001Type;

    constructor(public travelService: TravelService) {
        this.policyData = {};
        this.policyData.StartDate = new Date(Date.now());
        this.policyData.EndDate = new Date(2018, 5, 31);
        this.policyData.IndividualInsurance = {};
        let policyHolder: IPersonType = {};
        policyHolder.PersonRoles = [RoleTypeId.InsuredPerson, RoleTypeId.PolicyHolder,]
        policyHolder.NaturalPerson = {};
        policyHolder.NaturalPerson.IsResident = true;
        policyHolder.NaturalPerson.Addresses = [];
        let address: IAddressType = { AddressTypeId: AddressTypeIdType.ST, CountryId: 'RS', };
                    //{ Region: 'BG', HouseNumber: '11', Municipality: 'NBG', PostNumber: '11000', City:'Beograd', Street:'aaa', AddressTypeId: AddressTypeIdType.ST, CountryId: 'RS', };
        policyHolder.NaturalPerson.Addresses.push(address);
        this.policyData.Persons = [policyHolder];
    }

    public ngOnInit(): void {
    }

    public save(): void {

        if (this.policyData.IndividualInsurance != null) {
            let insuranceData = this.policyData.IndividualInsurance;
            insuranceData.Name = this.policyData.Persons[0].NaturalPerson.Name;
            insuranceData.Surname = this.policyData.Persons[0].NaturalPerson.Surname;
            insuranceData.DateOfBirth = this.policyData.Persons[0].NaturalPerson.DateOfBirth;
            insuranceData.PersonalId = this.policyData.Persons[0].NaturalPerson.PersonalId;
        }

        this.travelService.save(this.policyData).then((result: IZD0020001Type) => {
            this.policyData = result;
        }, (error: any) => {
            alert(error);
        });
    }
}