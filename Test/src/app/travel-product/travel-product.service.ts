﻿import { Headers, Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';

import { IZD0020001Type } from './domain/interfaces.gen';

@Injectable()
export class TravelService {
    private apiUrl: string = 'http://localhost/AdInsure.Triglav.Kopaonik.API.Server/api/public/v1/policies/ZD0020001/';

    constructor(private http: Http) {
    }

    public save(policyData: IZD0020001Type): Promise<IZD0020001Type> {        
        var headers = new Headers();
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Authorization', this.getAuthorizationHeader());

        return this.http.post(this.apiUrl + 'create-policy', policyData, {
            headers: headers
        })
            .toPromise()
            .then(response =>
                JSON.parse(response.text()) as IZD0020001Type
            )
            .catch(this.handleError);
    }

    //public print(policyid: string): Promise<string> {
    //    var headers = new headers();
    //    headers.append('access-control-allow-origin', '*');
    //    headers.append('authorization', this.getauthorizationheader());

    //    return this.http.post(this.apiurl + 'print', policyid, {
    //        headers: headers
    //    })
    //        .topromise()
    //        .then(response =>
    //            json.parse(response.text()) as izd0020001type
    //        )
    //        .catch(this.handleerror);
    //}

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); 
        return Promise.reject(error.message || error);
    }

    private getAuthorizationHeader(): string {
        let tokenData: any = JSON.parse(localStorage.getItem('token'));

        return tokenData.token_type + ' ' + tokenData.access_token;
    }

    
}