﻿/*
    // AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.AddressType
    export class AddressType implements IAddressType {
        public CountryId: string = undefined;
        public Region: string = undefined;
        public Municipality: string = undefined;
        public PostNumber: string = undefined;
        public City: string = undefined;
        public Street: string = undefined;
        public HouseNumber: string = undefined;
        public AddressTypeId: AddressTypeIdType = undefined;

        constructor() {
        }

    }

    // AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.ImportedPolicyBaseType
    export class ImportedPolicyBaseType implements IImportedPolicyBaseType {
        public PolicyId: string = undefined;
        public ProductId: string = undefined;
        public CreatedDate: Date = undefined;
        public StartDate: Date = undefined;
        public EndDate: Date = undefined;
        public AgentId: number = undefined;
        public Persons: PersonType[] = undefined;

        constructor() {
        }

    }

    // AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.PersonBaseType
    export class PersonBaseType implements IPersonBaseType {
        public TaxNumber: string = undefined;
        public IsResident: boolean = undefined;
        public CountryId: string = undefined;
        public Addresses: AddressType[] = undefined;

        constructor() {
        }

    }

    // AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.LegalPersonType
    export class LegalPersonType extends PersonBaseType implements ILegalPersonType {
        public VatNumber: string = undefined;
        public Title: string = undefined;

        constructor() {
            super();
        }

    }

    // AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.NaturalPersonType
    export class NaturalPersonType extends PersonBaseType implements INaturalPersonType {
        public Name: string = undefined;
        public Surname: string = undefined;
        public DateOfBirth: Date = undefined;
        public PersonalId: string = undefined;
        public Gender: Gender = undefined;

        constructor() {
            super();
        }

    }

    // AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.PersonType
    export class PersonType implements IPersonType {
        public PersonId: number = undefined;
        public PersonRoles: RoleTypeId[] = undefined;
        public NaturalPerson: NaturalPersonType = undefined;
        public LegalPerson: LegalPersonType = undefined;

        constructor() {
        }

    }
}

module AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Models {

    // AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.FamilyInsuranceType
    export class FamilyInsuranceType implements AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.IFamilyInsuranceType {
        public Description: string = undefined;
        public Rank: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.PolicyPersonRange = undefined;
        public PersonsList: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Models.PolicyPersonItem[] = undefined;
        public Package: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.IndividualPackage = undefined;
        public InsuranceMode: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.InsuranceMode = undefined;
        public Surcharges: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.BasicSurcharges = undefined;

        constructor() {
        }

    }

    // AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.GroupInsuranceType
    export class GroupInsuranceType implements AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.IGroupInsuranceType {
        public Description: string = undefined;
        public Rank: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.PolicyPersonRange = undefined;
        public PersonsList: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Models.PolicyPersonItem[] = undefined;
        public Package: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.GroupPackage = undefined;
        public InsuranceMode: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.InsuranceMode = undefined;
        public Surcharges: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.BasicSurcharges = undefined;

        constructor() {
        }

    }

    // AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.IndividualInsuranceType
    export class IndividualInsuranceType implements AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.IIndividualInsuranceType {
        public Name: string = undefined;
        public Surname: string = undefined;
        public DateOfBirth: Date = undefined;
        public PersonalId: string = undefined;
        public PassportNumber: string = undefined;
        public Description: string = undefined;
        public Package: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.IndividualPackage = undefined;
        public InsuranceMode: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.InsuranceMode = undefined;
        public Surcharges: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.Surcharges = undefined;

        constructor() {
        }

    }

    // AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.IndividualMultiEuroEntranceInsuranceType
    export class IndividualMultiEuroEntranceInsuranceType implements AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.IIndividualMultiEuroEntranceInsuranceType {
        public Name: string = undefined;
        public Surname: string = undefined;
        public DateOfBirth: Date = undefined;
        public PersonalId: string = undefined;
        public PassportNumber: string = undefined;
        public Description: string = undefined;
        public InsurancePeriodMonths: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.PeriodMonths = undefined;
        public InsurancePeriodDays: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.PeriodDays = undefined;
        public PersonsList: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Models.PolicyPersonItem[] = undefined;
        public Package: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.IndividualMultiEuroEntrancePackage = undefined;
        public Surcharges: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.Surcharges = undefined;

        constructor() {
        }

    }

    // AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.PolicyPersonItem
    export class PolicyPersonItem implements AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.IPolicyPersonItem {
        public Name: string = undefined;
        public Surname: string = undefined;
        public DateOfBirth: Date = undefined;
        public PersonalId: string = undefined;
        public PassportNumber: string = undefined;
        public IsAthlete: boolean = undefined;
        public AdditionalDescription: string = undefined;

        constructor() {
        }

    }

    // AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.StudentsInsuranceType
    export class StudentsInsuranceType implements AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.IStudentsInsuranceType {
        public Description: string = undefined;
        public Rank: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.PolicyPersonRange = undefined;
        public PersonsList: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Models.PolicyPersonItem[] = undefined;
        public Package: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.BasicPackage = undefined;
        public InsuranceMode: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.InsuranceMode = undefined;
        public Surcharges: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.BasicSurcharges = undefined;

        constructor() {
        }

    }

    // AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.ZD0020001Type
    export class ZD0020001Type extends ImportedPolicyBaseType implements AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Interfaces.IZD0020001Type {
        public IndividualInsurance: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Models.IndividualInsuranceType = undefined;
        public FamilyInsurance: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Models.FamilyInsuranceType = undefined;
        public GroupInsurance: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Models.GroupInsuranceType = undefined;
        public StudentsInsurance: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Models.StudentsInsuranceType = undefined;
        public IndividualMultiEuroEntranceInsurance: AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Models.IndividualMultiEuroEntranceInsuranceType = undefined;

        constructor() {
            super();
        }

    }
*/