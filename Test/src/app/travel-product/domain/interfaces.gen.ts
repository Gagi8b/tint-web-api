﻿import { IImportedPolicyBaseType } from  './common-interfaces.gen'

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.BasicPackage
export enum BasicPackage {
    EuropeTurkeyTunisAndEgypt = 0,
    BulgariaTurkeyTunisAndEgypt = 1,
    WholeWorld = 2
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.BasicSurcharges
export enum BasicSurcharges {
    ConstructionWorker = 0,
    TemporaryWorkingAbroad = 1
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.GroupPackage
export enum GroupPackage {
    EuropeTurkeyTunisAndEgypt = 0,
    BulgariaTurkeyTunisAndEgypt = 1,
    WholeWorld = 2,
    WholeWorldExceptUSACanadaAustraliaAndJapan = 3,
    EuropeTurkeyTunisAndEgyptViaTouristOrganizations = 4
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.IndividualMultiEuroEntrancePackage
export enum IndividualMultiEuroEntrancePackage {
    WholeWorld = 0,
    Europe = 1
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.IndividualPackage
export enum IndividualPackage {
    EuropeTurkeyTunisAndEgypt = 0,
    BulgariaTurkeyTunisAndEgypt = 1,
    WholeWorld = 2,
    WholeWorldExceptUSACanadaAustraliaAndJapan = 3
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.InsuranceMode
export enum InsuranceMode {
    Basic = 0,
    Premium = 1
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.PeriodDays
export enum PeriodDays {
    Days005 = 0,
    Days010 = 1,
    Days015 = 2,
    Days020 = 3,
    Days030 = 4,
    Days045 = 5,
    Days060 = 6,
    Days090 = 7,
    Days180 = 8
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.PeriodMonths
export enum PeriodMonths {
    Months01 = 0,
    Months02 = 1,
    Months03 = 2,
    Months06 = 3,
    Months12 = 4
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.PolicyPersonRange
export enum PolicyPersonRange {
    Up_To_100 = 0,
    Between_101_And_200 = 1,
    Between_201_And_300 = 2,
    Above_300 = 3
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.Surcharges
export enum Surcharges {
    ConstructionWorker = 0,
    TemporaryWorkingAbroad = 1,
    ProffesionalAthlete = 2,
    Athlete = 3
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.FamilyInsuranceType
export interface IFamilyInsuranceType {
    Description: string;
    Rank: PolicyPersonRange;
    PersonsList: IPolicyPersonItem[];
    Package: IndividualPackage;
    InsuranceMode: InsuranceMode;
    Surcharges: BasicSurcharges;
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.GroupInsuranceType
export interface IGroupInsuranceType {
    Description: string;
    Rank: PolicyPersonRange;
    PersonsList: IPolicyPersonItem[];
    Package: GroupPackage;
    InsuranceMode: InsuranceMode;
    Surcharges: BasicSurcharges;
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.IndividualInsuranceType
export interface IIndividualInsuranceType {
    Name?: string;
    Surname?: string;
    DateOfBirth?: Date;
    PersonalId?: string;
    PassportNumber?: string;
    Description?: string;
    Package?: IndividualPackage;
    InsuranceMode?: InsuranceMode;
    Surcharges?: Surcharges;
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.IndividualMultiEuroEntranceInsuranceType
export interface IIndividualMultiEuroEntranceInsuranceType {
    Name: string;
    Surname: string;
    DateOfBirth: Date;
    PersonalId: string;
    PassportNumber: string;
    Description: string;
    InsurancePeriodMonths: PeriodMonths;
    InsurancePeriodDays: PeriodDays;
    PersonsList: IPolicyPersonItem[];
    Package: IndividualMultiEuroEntrancePackage;
    Surcharges: Surcharges;
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.PolicyPersonItem
export interface IPolicyPersonItem {
    Name: string;
    Surname: string;
    DateOfBirth: Date;
    PersonalId: string;
    PassportNumber: string;
    IsAthlete: boolean;
    AdditionalDescription: string;
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.StudentsInsuranceType
export interface IStudentsInsuranceType {
    Description: string;
    Rank: PolicyPersonRange;
    PersonsList: IPolicyPersonItem[];
    Package: BasicPackage;
    InsuranceMode: InsuranceMode;
    Surcharges: BasicSurcharges;
}

// AdInsure.Triglav.Kopaonik.API.Product.Mapping.Public.v1.ZD0020001.Generated.ZD0020001Type
export interface IZD0020001Type extends IImportedPolicyBaseType {
    IndividualInsurance?: IIndividualInsuranceType;
    FamilyInsurance?: IFamilyInsuranceType;
    GroupInsurance?: IGroupInsuranceType;
    StudentsInsurance?: IStudentsInsuranceType;
    IndividualMultiEuroEntranceInsurance?: IIndividualMultiEuroEntranceInsuranceType;
}