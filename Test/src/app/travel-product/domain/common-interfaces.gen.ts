﻿export interface ICTItem<T> {
    Id: T;
    Description: string;
}

// AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.AddressTypeIdType
export enum AddressTypeIdType {
    DO = 0,
    SE = 1,
    XN = 2,
    SN = 3,
    ST = 4,
    ZA = 5
}

// AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.Gender
export enum Gender {
    Neutral = 0,
    Male = 1,
    Female = 2
}

// AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.RoleTypeId
export enum RoleTypeId {
    PolicyHolder = 0,
    InsuredPerson = 1,
    LinkedPerson = 2,
    Payer = 3,
    InvoicePayer = 4,
    User = 5
}

// AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.AddressType
export interface IAddressType {
    CountryId?: string;
    Region?: string;
    Municipality?: string;
    PostNumber?: string;
    City?: string;
    Street?: string;
    HouseNumber?: string;
    AddressTypeId?: AddressTypeIdType;
}

// AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.ImportedPolicyBaseType
export interface IImportedPolicyBaseType {
    PolicyId?: string;
    ProductId?: string;
    CreatedDate?: Date;
    StartDate?: Date;
    EndDate?: Date;
    AgentId?: number;
    Persons?: IPersonType[];
}

// AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.PersonBaseType
export interface IPersonBaseType {
    TaxNumber?: string;
    IsResident?: boolean;
    CountryId?: string;
    Addresses?: IAddressType[];
}

// AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.LegalPersonType
export interface ILegalPersonType extends IPersonBaseType {
    VatNumber: string;
    Title: string;
}

// AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.NaturalPersonType
export interface INaturalPersonType extends IPersonBaseType {
    Name?: string;
    Surname?: string;
    DateOfBirth?: Date;
    PersonalId?: string;
    Gender?: Gender;
}

// AdInsure.Triglav.API.Product.Mapping.Public.v1.Generated.PersonType
export interface IPersonType {
    PersonId?: number;
    PersonRoles?: RoleTypeId[];
    NaturalPerson?: INaturalPersonType;
    LegalPerson?: ILegalPersonType;
}