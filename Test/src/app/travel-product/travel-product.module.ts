﻿import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
    MdButtonModule, MdInputModule, MdOptionModule, MdSelectModule, MdDatepickerModule, MdNativeDateModule, MdCheckboxModule, MdCardModule
} from '@angular/material';

import { TravelPolicyComponent } from './travel-policy.component';
import { PersonsPolicyComponent } from './policy-persons.component';
import { PolicyBaseComponent } from './policy-base.component';
import { TravelIndividualComponent } from './insurance-components/travel-individual.component'

@NgModule({
    declarations: [
        TravelPolicyComponent,
        TravelIndividualComponent,
        PolicyBaseComponent,
        PersonsPolicyComponent
    ],
    imports: [
        HttpModule,
        FormsModule,
        CommonModule,
        MdButtonModule, MdInputModule, MdOptionModule, MdSelectModule, MdDatepickerModule, MdNativeDateModule, MdCheckboxModule, MdCardModule
    ],
    exports: [
        TravelPolicyComponent,
        TravelIndividualComponent,
        PolicyBaseComponent,
        PersonsPolicyComponent
    ]
})
export class TravelProductModule {
}