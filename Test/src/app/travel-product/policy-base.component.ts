﻿import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MdDatepicker } from '@angular/material';

import { IZD0020001Type } from './domain/interfaces.gen';
import { IPersonType, Gender, ICTItem } from './domain/common-interfaces.gen';

@Component({
    selector: 'policy-base-component',
    templateUrl: './policy-base.component.html',
    providers: []
})
export class PolicyBaseComponent implements OnInit {
    @ViewChild(MdDatepicker) datepicker: MdDatepicker<Date>;
    @Input() policy: IZD0020001Type;

    public persons: IPersonType[];
    public genders: ICTItem<Gender>[] = [];

    constructor() {
        this.genders.push({ Id: Gender.Male, Description: 'Male' });
        this.genders.push({ Id: Gender.Female, Description: 'Female' });
        this.genders.push({ Id: Gender.Neutral, Description: 'Neutral' });
    }

    ngOnInit(): void {
        this.persons = this.policy.Persons;
    }
}