﻿import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MdDatepicker } from '@angular/material';

import { IZD0020001Type, IIndividualInsuranceType, IndividualPackage, InsuranceMode, Surcharges } from '../domain/interfaces.gen';
import { ICTItem } from '../domain/common-interfaces.gen';

@Component({
    selector: 'travel-individual-component',
    templateUrl: './travel-individual.component.html',
    providers: []
})
export class TravelIndividualComponent implements OnInit {
    @ViewChild(MdDatepicker) datepicker: MdDatepicker<Date>;
    @Input() policy: IZD0020001Type;

    public insuranceData: IIndividualInsuranceType;
    public packages: ICTItem<IndividualPackage>[] = [];
    public insuranceModes: ICTItem<InsuranceMode>[] = [];
    public surcharges: ICTItem<Surcharges>[] = [];
    public surchargesChecked: boolean[] = [false, false, false, false];

    constructor() {
        this.packages.push({
            Id: IndividualPackage.EuropeTurkeyTunisAndEgypt, Description : "Europe, Turkey, Tunis and Egypt"
        });
        this.packages.push({
            Id: IndividualPackage.BulgariaTurkeyTunisAndEgypt, Description: "Bulgaria, Turkey, Tunis and Egypt"
        });
        this.packages.push({
            Id: IndividualPackage.WholeWorld, Description: "Whole world"
        });
        this.packages.push({
            Id: IndividualPackage.WholeWorldExceptUSACanadaAustraliaAndJapan, Description: "Whole world except USA, Canada, Australia, and Japan"
        });

        this.insuranceModes.push({
            Id: InsuranceMode.Basic, Description: "Basic"
        });
        this.insuranceModes.push({
            Id: InsuranceMode.Premium, Description: "Premium"
        });

        this.surcharges.push({
            Id: Surcharges.ConstructionWorker, Description: "Construction worker"
        });
        this.surcharges.push({
            Id: Surcharges.TemporaryWorkingAbroad, Description: "Temporary working abroad"
        });
        this.surcharges.push({
            Id: Surcharges.ProffesionalAthlete, Description: "Proffesional athlete"
        });
        this.surcharges.push({
            Id: Surcharges.Athlete, Description: "Athlete"
        });
    }

    ngOnInit(): void {
        this.insuranceData = this.policy.IndividualInsurance;

        //this.insuranceData.Name = this.policy.Persons[0].NaturalPerson.Name;
        //this.insuranceData.Surname = this.policy.Persons[0].NaturalPerson.Surname;
        //this.insuranceData.DateOfBirth = this.policy.Persons[0].NaturalPerson.DateOfBirth;
        this.insuranceData.Description = "Individualno putno osiguranje";
        //this.insuranceData.PersonalId = this.policy.Persons[0].NaturalPerson.PersonalId;
        
    }

    surchargeChanged(value: any, event: any): void {
        //this.surchargesChecked.forEach((v, i, a) => this.reSetSurchargesChecked(v, i, value));

        this.surchargesChecked = [false, false, false, false];
        this.insuranceData.Surcharges = null;
        if (event.checked)
        {
            this.surchargesChecked[value] = true;
            this.insuranceData.Surcharges = value;
        }
    }

    //reSetSurchargesChecked(value: boolean, index: number, clickedIndex: number): void
    //{
    //    if (index == clickedIndex)
    //    {
    //        value = !value
    //    }
    //    else
    //    {
    //        value = false;
    //    }
    //}
}