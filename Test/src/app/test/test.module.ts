﻿import { HttpModule } from '@angular/http';
import { TestComponent } from './test.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        TestComponent
    ],
    imports: [
        HttpModule,
        CommonModule
    ],
    exports: [
        TestComponent
    ]
})
export class TestModule {
}