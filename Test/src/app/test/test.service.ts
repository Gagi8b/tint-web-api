﻿import { Headers, Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { TestData } from './test-data';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class TestService {

    private apiUrl: string = 'http://localhost:3000/api/data';

    constructor(private http: Http) {
    }

    public getTestData(): Promise<TestData[]> {
        //var ret = Promise.resolve([
        //    { "Id": 1, "Description": 'aa' },
        //    { "Id": 2, "Description": 'bb' }
        //]);
        //return ret;

        return this.http.get(this.apiUrl)
            .toPromise()
            .then(response =>
                JSON.parse(response.text()) as TestData[]
            )
            .catch(this.handleError);        
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}