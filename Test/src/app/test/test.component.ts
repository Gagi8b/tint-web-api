﻿import { Component, OnInit } from '@angular/core';
import { TestService } from './test.service';
import { TestData } from './test-data';

@Component({
    selector: 'test',
    templateUrl: './test.component.html',
    providers: [TestService]
})
export class TestComponent implements OnInit {
    public data: TestData[];
    public selectedData: TestData;

    constructor(private testService: TestService) {
    }

    ngOnInit(): void {
        this.testService.getTestData().then(value => this.data = value);
    }

    public onSelect(d: TestData): void {
        this.selectedData = d;
    }
}